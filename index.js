// Example express application adding the parse-server module to expose Parse
// compatible API routes.

var express = require('express');
var path = require('path');
var crypto = require('crypto');
var bodyParser = require('body-parser');
var stripe = require('stripe')('sk_test_rwXpFKat6zsIloS4eGDTGfLW');

//plain comment, ignore

var app = express();

// Serve static assets from the /public folder
app.use('/public', express.static(path.join(__dirname, '/public')));

// Parse Server plays nicely with the rest of your web routes
app.get('/', function(req, res) {
  res.status(200).send('Welcome Alien ! You are monitored');
});

app.use(bodyParser.json({limit: '5mb'}));
app.use(bodyParser.urlencoded({limit: '5mb', extended: true}));

app.use('/vajidtest', function(req,res){

    console.log(req.body);
    var name = req.body.name;
    if(name)
      return res.json("test name is "+name);
    else
      return res.json("name not found in request");
});

app.use('/chargecard', function(req,res){

    console.log(req.body);
    var amount = req.body.amount;
    var token = req.body.token;
    var description = req.body.description;
    if(amount && token && description){
        chargeCard(amount,token,description,function(err,result){
            if(err){
               return res.status(500).send(err);
            }else{
              return res.send(result);
            }
        });
    }else{
      return res.status(500).send("Amount, token and description are mandatory parameters. Please provide valid inputs");
    }
});

function chargeCard(amount,token,description,callback){

  console.log("reached inside the function");

    stripe.charges.create({
      amount: amount,
      currency: "usd",
      source: token,
      description: description
    }, function(err, charge) {
        if(err){
          console.log("It was an error");
          console.log(err);
          return callback(err,null);
          // return "Charging failed, sorry";
          // return res.status(500).send({ result: 'error' : response : err});
        }else{
          console.log("It was success");
          console.log(charge);
          return callback(null,charge);
          // return "charging is done, thanks";
          // return res.status(500).send({ result: 'error' : response : err});
        }
    });
}

// There will be a test page available on the /test path of your server url
// Remove this before launching your app
app.get('/test', function(req, res) {
  res.sendFile(path.join(__dirname, '/public/test.html'));
});

var port = process.env.PORT || 80;
var httpServer = require('http').createServer(app);
httpServer.listen(port, function() {
    console.log('Stripe server running on port ' + port + '.');
});
